

$('#meb_name').on('keyup',function()
{
	//alert(name);
	val =$(this).val();
	if(val.match(/^[a-zA-Z]+$/))
	{
		$('#errormeb_name').hide();
		$(this).css('border-color','');
	}
	else
	{
		$(this).css('border-color','red');
		$('#errormeb_name').show();
		$('#errormeb_name').html('Enter valid Name');
		$('#errormeb_name').css('color','red');
	}
});	

$('#c_lname').on('keyup',function()
{
	//alert(name);
	val =$(this).val();
	if(val.match(/^[a-zA-Z]+$/))
	{
		$('#errorc_lname').hide();
		$(this).css('border-color','');
	}
	else
	{
		$(this).css('border-color','red');
		$('#errorc_lname').show();
		$('#errorc_lname').html('Enter valid Last Name');
		$('#errorc_lname').css('color','red');
	}
});	



$('#meb_email').on('change',function()
{
	val =$(this).val();
	if(val.match(/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i))
	{
		$('#errormeb_email').hide();
		$(this).css('border-color','');
	}
	else
	{
		$(this).css('border-color','red');
		$('#errormeb_email').show();
		$('#errormeb_email').html('Enter valid Email');
		$('#errormeb_email').css('color','red');
	}
});
$('#meb_mobile').on('keyup',function()
{
	val =$(this).val();
	if(val.match(/^[7/8/9][0-9]{9}$/))
	{
		$('#errormeb_mobile').hide();
		$(this).css('border-color','');
	}
	else
	{
		$(this).css('border-color','red');
		$('#errormeb_mobile').show();
		$('#errormeb_mobile').html('Enter valid Mobile');
		$('#errormeb_mobile').css('color','red');
	}
});


$('#meb_phone').on('keyup',function()
{
	val =$(this).val();
	if(val.match(/^[7/8/9][0-9]{9}$/))
	{
		$('#errormeb_phone').hide();
		$(this).css('border-color','');
	}
	else
	{
		$(this).css('border-color','red');
		$('#errormeb_phone').show();
		$('#errormeb_phone').html('Enter valid Phone');
		$('#errormeb_phone').css('color','red');
	}
});

