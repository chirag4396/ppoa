
$('#frmvideo').on('submit', function(e){
    e.preventDefault();
$('#success_message').fadeIn().html('Checking Data');
$("#btnadd").prop('disabled', true);

var fd = new FormData(this);
$.ajax({
    url: 'add_video.php',
    type: 'POST',
    data:  fd,
    contentType: false,
    processData:false,
    success: function(data) 
    {
        //console.log(data);
          
            $('#success_message').fadeIn().html('Data Submitted sucessfully');
            setTimeout(function() {
                $('#frmvideo')[0].reset();
                $("#btnadd").prop('disabled', false);
                $('#success_message').fadeOut("slow");
            }, 1000 );
        },
    });
});