
<?php
session_start();
if(!$_SESSION['uname']){
	header('location:index.php');
}
?>
<!DOCTYPE html>
<head>
	<title>PPOA| Home :: w3layouts</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="Colored Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
	Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
	<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
	<!-- bootstrap-css -->
	<link rel="stylesheet" href="css/bootstrap.css">
	<!-- //bootstrap-css -->
	<!-- Custom CSS -->
	<link href="css/style.css" rel='stylesheet' type='text/css' />
	<!-- font CSS -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
	<!-- font-awesome icons -->
	<link rel="stylesheet" href="css/font.css" type="text/css"/>
	<link href="css/font-awesome.css" rel="stylesheet"> 
	<!-- //font-awesome icons -->
	<script src="js/jquery2.0.3.min.js"></script>
	<script src="js/modernizr.js"></script>
	<script src="js/jquery.cookie.js"></script>
	<script src="js/screenfull.js"></script>
	<script>
		$(function () {
			$('#supported').text('Supported/allowed: ' + !!screenfull.enabled);

			if (!screenfull.enabled) {
				return false;
			}

			

			$('#toggle').click(function () {
				screenfull.toggle($('#container')[0]);
			});	
		});
	</script>
	<!-- charts -->
	<script src="js/raphael-min.js"></script>
	<script src="js/morris.js"></script>
	<link rel="stylesheet" href="css/morris.css">
	<!-- //charts -->
	<!--skycons-icons-->
	<script src="js/skycons.js"></script>
	<!--//skycons-icons-->
</head>
<body class="dashboard-page">
	<script>
		var theme = $.cookie('protonTheme') || 'default';
		$('body').removeClass (function (index, css) {
			return (css.match (/\btheme-\S+/g) || []).join(' ');
		});
		if (theme !== 'default') $('body').addClass(theme);
	</script>
	<?php require("nav_menu.php"); ?>

	<section class="wrapper scrollable">
		<nav class="user-menu">
			<a href="javascript:;" class="main-menu-access">
				<i class="icon-proton-logo"></i>
				<i class="icon-reorder"></i>
			</a>
		</nav>
		<?php require("header.php");?>
		<div class="main-grid">
			
			
			<div class="social grid">
				<div class="grid-info">
					<div class="col-md-3 top-comment-grid">
						<div class="comments likes">
							<div class="comments-icon">
								<i class="fa fa-camera-retro fa-5x" style="color:white"></i>
							</div>
							<div class="comments-info likes-info">
								<h3></h3>
								<a href="#">Gallary</a>
							</div>
							<div class="clearfix"> </div>
						</div>
					</div> 
					

					

					
					<div class="col-md-6 top-comment-grid">
						<div class="comments views">
							<div class="comments-icon">
								<i class="fa fa-envelope fa-5x" style="color:white;" ></i>
							</div>
							<div class="comments-info tweets-info">
								<h3></h3>
								<a href="#">Message</a>
							</div>
							<div class="clearfix"> </div>
						</div>
					</div>
					
					
					
					<div class="clearfix"> </div>
				</div>
			</div>

			<div class="agile-grids">
				<div class="col-md-4 charts-right">
					
				</div>
				<div class="col-md-8 chart-left">
					
				</div>
				<div class="clearfix"> </div>
			</div>
			
			<div class="agile-two-grids">
				<div class="col-md-6 count">
					
				</div>
				<div class="col-md-6 weather">
					
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
		<!-- footer -->
		<?php require("footer.php") ?>

		<!-- //footer -->
	</section>
	<script src="js/bootstrap.js"></script>

</body>
</html>
