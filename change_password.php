<?php

session_start();
include("config.php");
?>    
<!doctype html>

<html lang="en">
<head>
  <title> PPOA | Welcome to PPOA</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <!-- Page Description and Author -->
  <meta name="description" content="Pawar Travels">
  <meta name="author" content="Amol">

  <!-- Bootstrap CSS  -->
  <link rel="stylesheet" href="assets/css/bootstrap.min.css" type="text/css" media="screen">

  <link rel="stylesheet" href="assets/fonts/font-awesome.min.css" type="text/css" media="screen">   
  
  <link rel="stylesheet" type="text/css" href="assets/css/main.css" media="screen">

  <link rel="stylesheet" type="text/css" href="assets/css/animate.css" media="screen">   

  <style>
    #portfolio-list .mix {
      padding-left: 15px!important;
      padding-right: 15px!important;
      margin-bottom: 25px!important;
    }
  </style>

</head>
<body>
  <div id="container">
    <!-- Start Header Section -->
    <header id="header-wrap" class="site-header clearfix">
      <!-- Start Top Bar -->
      <div class="top-bar hidden-xs">
        <div class="container">
          <div class="row">
            <div class="col-md-7 col-sm-9">
              <!-- Start Contact Info -->
              <ul class="contact-details">
                <li>
                  <a href="#">
                    <i class="icon-envelope">
                    </i>
                    contact@pressownerspune.org
                  </a>
                </li>
                <li>
                  <a href="#">
                    <i class="icon-call-out">
                    </i>
                    Call Us: +91-20-24471777 / 24430460
                  </a>
                </li>  
                <li>
                  <a href="#">
                    <i class="icon-clock">
                    </i>
                    Sun-Sat <span class="timing">(9am-5pm)</span>
                  </a>
                </li>                
              </ul>
              <!-- End Contact Info -->
            </div>
            <div class="col-md-5 col-sm-3">
              <!-- Start Social Links -->
              <ul class="social-list">
                <li>
                  <a href="#" class="social-link facebook" data-toggle="tooltip" data-placement="bottom" title="Facebook" href="#"><i class="fa fa-facebook"></i></a>
                </li>
                <li>
                  <a href="#" class="social-link twitter" data-toggle="tooltip" data-placement="bottom" title="Twitter" href="#"><i class="fa fa-twitter"></i></a>
                </li>
                <li>
                  <a href="#" class="social-link google" data-toggle="tooltip" data-placement="bottom" title="Google Plus" href="#"><i class="fa fa-google-plus"></i></a>
                </li>

              </ul>
              <!-- End Social Links -->
            </div>
          </div>
        </div>
      </div>
      <!-- End Top Bar -->

      <!-- Start  Logo & Navigation  -->
      <div class="navbar navbar-default navbar-top" role="navigation" data-spy="affix" data-offset-top="50">
        <div class="container">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <!-- Stat Toggle Nav Link For Mobiles -->
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <!-- End Toggle Nav Link For Mobiles -->
            <div class="logo-wrapper">
              <a class="navbar-brand" href="index.html">
                <img src="assets/img/logo.jpg" alt="ConBiz">
              </a>  
            </div>
          </div>
          <!--  Brand and toggle menu for mobile ends  -->

          <div class="navbar-collapse collapse">
            <!-- Stat Search -->
            <div class="search-side">
              <a class="show-search">
                <i class="icon-magnifier"></i>
              </a>
            </div>
            <!-- Form for navbar search area -->
            <form class="full-search">
              <div class="container">
                <div class="row">
                  <input class="form-control" type="text" placeholder="Search...">
                  <a class="close-search">
                    <span class="fa fa-times fa-2x">
                    </span>
                  </a>
                </div>
              </div>
            </form>
            <!-- Search form ends -->

            <!-- Start Navigation List -->
            <ul class="nav navbar-nav navbar-right">
              <li><a href="index.html">Home </a> </li>                
              <li><a href="#"> Company </a>
                <ul class="dropdown">
                  <li><a href="about.html">About Us</a></li>                    
                  <li><a href="history.html">Our History</a></li>
                  <li><a class="active" href="directors.html">Our Directors</a></li>
                  <li><a href="presidents.html">List of Presidents</a></li>
                  <li><a href="members.php">List of Members</a></li>                    
                </ul>
              </li>
              <li><a href="products.html">Products</a></li>
              <li><a href="gallery.html">Gallery</a> </li>
              <li><a href="events.html">Events</a></li>                
              <li><a href="contact.html">Contact</a></li>
              <li><a href="login.php" class="active" >Login</a></li>                
            </ul>
            <!-- End Navigation List -->
          </div>
        </div>

        <!-- Mobile Menu Start -->
        <ul class="wpb-mobile-menu">
          <li><a href="index.html">Home </a> </li>                
          <li><a class="active" href="#"> Company </a>
            <ul class="dropdown">
              <li><a href="about.html">About Us</a></li>                    
              <li><a href="history.html">Our History</a></li>
              <li><a class="active" href="directors.html">Our Directors</a></li>
              <li><a href="presidents.html">List of Presidents</a></li>
              <li><a href="members.php">List of Members</a></li>                    
            </ul>
          </li>
          <li><a href="products.html">Products</a></li>
          <li><a href="gallery.html">Gallery</a> </li>
          <li><a href="events.html">Events</a></li>                
          <li><a href="contact.html">Contact</a></li>
          <li><a href="login.php">Login</a></li>
        </ul>
        <!-- Mobile Menu End -->
      </div>
      <!-- End Header Logo & Navigation -->
    </header>
    <!-- End Header Section -->

    <!-- login content section start -->
    <div class="login-area" style="padding-bottom: 50px;">
      <div class="container">
        <div class="row">

          <div class="col-md-6 col-xs-12  col-md-offset-3">
            <div class="tb-login-form res">
              <h5 class="tb-title">Change Password </h5>
              <form action="#" method="post" id="frmchnge" name="frmchnge">
                <p class="checkout-coupon top log a-an">
                  <label class="l-contact">
                    Email Address
                    <em>*</em>
                  </label>
                  <input type="email" name="meb_email" id="meb_email" value="<?php ?>" />
                </p>

                <p class="checkout-coupon top-down log a-an">
                  <label class="l-contact">
                    Current  Password
                    <em>*</em>
                  </label>
                  <input type="password" name="current_password" id="current_password">
                </p>
                <p class="checkout-coupon top-down log a-an">
                  <label class="l-contact">
                    New Password
                    <em>*</em>
                  </label>
                  <input type="password" name="new_password" id="new_password">
                </p>
                <p class="checkout-coupon top-down log a-an">
                  <label class="l-contact">
                    Confirm Password
                    <em>*</em>
                  </label>
                  <input type="password" name="confirm_password" id="confirm_password">
                </p>
                <p class="login-submit5 ress">
                  <input value="Submit" class="button-primary " type="submit" id="btnchange" name="btnchange">
                </p>
              </form>
                        
                      </div>
                    </div>
                  </div>
                </div>
                <!-- login  content section end -->


                <!-- Start Footer Section -->
                <footer class="footer">
                  <div class="container">
                    <div class="row footer-widgets">
                      <!-- Start Contact Widget -->
                      <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="footer-widget contact-widget">
                          <h4>
                            <img src="assets/img/logo.jpg" class="img-responsive" alt="Footer Logo" width="100%" />
                          </h4>
                          <p>PPOA Limited's Corporate Identification Number is (CIN) U22219PN1952PLC008970 and its registration number is 8970.  </p>
                        </div>
                      </div>
                      <!-- End Contact Widget -->

                      <div class="col-lg-3 col-sm-4">
                        <div class="footer-widget" data-wow-duration="2s" data-wow-delay=".5s">
                          <h4>
                            Our Company
                          </h4>
                          <ul class="page-footer-list">
                            <li>
                              <i class="fa fa-angle-right"></i>
                              <a href="history.html">Our History</a>
                            </li>
                            <li>
                              <i class="fa fa-angle-right"></i>
                              <a href="directors.html">Our Directors</a>
                            </li>
                            <li>
                              <i class="fa fa-angle-right"></i>
                              <a href="presidents.html">List of Presidents</a>
                            </li>
                            <li class="last-itm">
                              <i class="fa fa-angle-right"></i>
                              <a href="members.php">List of Members</a>
                            </li>                
                          </ul>
                        </div>
                      </div>

                      <div class="col-lg-3 col-sm-4">
                        <div class="footer-widget" data-wow-duration="2s" data-wow-delay=".5s">
                          <h4>
                            Our Company
                          </h4>
                          <ul class="page-footer-list">
                            <li>
                              <i class="fa fa-angle-right"></i>
                              <a href="about.html">About PPOA</a>
                            </li>
                            <li>
                              <i class="fa fa-angle-right"></i>
                              <a href="products.html">Our Products</a>
                            </li>
                            <li>
                              <i class="fa fa-angle-right"></i>
                              <a href="events.html">Our Events</a>
                            </li>
                            <li class="last-itm">
                              <i class="fa fa-angle-right"></i>
                              <a href="gallery.html">Gallery</a>
                            </li>                
                          </ul>
                        </div>
                      </div>

                      <!-- Start Address Widget -->
                      <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="footer-widget">
                          <h4>
                            PPOA Office Address
                          </h4>
                          <ul class="address">
                            <li>
                              <a href="#"><i class="fa fa-map-marker"></i><strong>Address (Regd. Office) : </strong>
                                'Pune Mudran Vidyalaya Bhavan', 209/B-4, Navi Peth, Pune-411030.</a>
                              </li>
                              <li>
                                <a href="#"><i class="fa fa-phone"></i> +91-20-24471777 / 24430460</a>
                              </li>
                              <li>
                                <a href="#"><i class="fa fa-envelope"></i> contact@pressownerspune.org</a>
                              </li>
                            </ul>
                          </div>
                        </div>
                        <!-- End Address Widget -->
                      </div>
                      <!-- .row -->        
                    </div>
                  </footer>
                  <!-- End Footer Section -->

                  <!-- Start Copyright -->
                  <div class="copyright-section">
                    <div class="container">
                      <div class="row">
                        <div class="col-sm-12">
                          <p>
                            Copyright &copy; 2018 PPOA - Designed & Developed by 
                            <a href="http://sungare.com">
                              Sungare Technologies Pvt. Ltd.
                            </a>
                          </p>
                        </div>
                      </div>
                      <!-- .row -->
                    </div>
                  </div>
                  <!-- End Copyright -->
                </div>
                <!-- End Full Body Container -->

                <!-- Go To Top Link -->
                <a href="#" class="back-to-top">
                  <i class="fa fa-angle-up"></i>
                </a>

                <!-- Start Loader -->
                <div id="loader">
                  <div class="square-spin">
                    <div></div>
                  </div>
                </div>



                <!-- Modal -->
                <div id="quick-view" class="quick-view modal fade" role="dialog">
                  <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">

                      <div class="modal-footer" data-dismiss="modal">
                        <span>x</span>
                      </div>

                      <div class="modal-body">
                       <div class="row">
                        <div class="col-xs-12 col-sm-4">
                          <div class="quick-image">
                            <div class="single-quick-image tab-content text-center" style="box-shadow: 2px 2px 5px 2px #e0dede;">
                              <div class="tab-pane  fade in active" id="sin-pro-1">
                                <img src="assets/img/team/demo.jpg" alt="" width="100%" />
                              </div>                              
                            </div>

                          </div>              
                        </div>
                        <div class="col-xs-12 col-sm-8">
                          <div class="quick-right">
                            <div class="quick-right-text">
                              <h3><strong>Mr. Ajay Gujarathi</strong></h3>
                              <div class="rating" style="margin-bottom: 2em;">                              

                                <div class="col-md-12">
                                  <i class="fa fa-envelope"style="margin-right:12px;"></i><a href="#" style="color:#888!important; font-size: 15px;">ppoa@ajaygroup.in</a>
                                </div>
                              </div>
                              <div class="amount">
                              </div>
                              <p style="border-bottom: none; margin-bottom: 0px;">You have to have a strong leader setting a direction. And it doesn’t even have to be the best direction-just a strong, clear one.</p>
                              <p style="border-bottom: none;margin-bottom: 0px;">An alumnus of the National Model School of the iron fisted Principal N. D. Nagarwala and a Fellow of Nowrosjee Wadia College, Director (1996-2003 and 2016-till date) Ajay Gujarathi is responsible for many strategic Decisions in The Poona Press Owners Association Ltd. </p>

                              <p style="border-bottom: none;margin-bottom: 0px;">
                                Born in Pune on 13th October in a very well-known business family known as the Sugar & Oil Kings – Narayandas Govinddas and Chimanlal Govinddas, He started his printing career in 1987 by incorporating Ajay Offsets Pvt. Ltd. and still continues to be its guiding force. </p>    
                                <p style="border-bottom: none;margin-bottom: 0px;">He is a multi-faced person and is known for his leadership and administration.</p>
                                <p style="border-bottom: none;margin-bottom: 0px;">He has served as President and Secretary of the Royal Connaught Boat Club for many years. He has also served on many Lions District Cabinet Posts. He is a Life member of the International Association of Lions. He is also associated and a member of Pune Gujarati Bandhu Samaj, The Pune Gujarati Kelavani Mandal, Punjabi Cultural Association, Mahratta Chamber Of Commerce, Ladies Club Ltd., Deccan Gymkhana Club, P. Y. C. Hindu Gymkhana, Royal Western India Turf Club, The New Club (Poona) Ltd.,Western India Automobile Association, Y. M. C. A. etc.</p>
                                <p style="border-bottom: none;margin-bottom: 0px;">He is also the recipient of many prestigious awards.</p>

                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                    </div>
                  </div>
                </div>

                <!-- Main JS  -->
                <script type="text/javascript" src="assets/js/jquery-2.1.4.min.js"></script>
                <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>    
                <script type="text/javascript" src="assets/js/main.js"></script>
              </body>
              </html>
              <?php

              if(isset($_POST['btnchange']))
              {
               $meb_email=$_POST['meb_email'];
               $current_password=$_POST['current_password'];
               $new_password=$_POST['new_password'];
               $confirm_password=$_POST['confirm_password'];
               $sql=("SELECT * FROM member_details WHERE meb_email='".$meb_email."'");
               $res=$conn->query($sql);
               $user=$res->fetch_assoc();

               // print_r($user);
               if($new_password === $confirm_password){                
                 if($user['meb_password'] === $current_password){
                  $updateQry ="update member_details set meb_password='".$new_password."' where meb_email ='".$meb_email."'";
                  $up = $conn->query($updateQry);
                  if($up){
                    //echo 'Sucessfully';
                    echo  "<script>alert('Password Changed  Sucessfully'); window.location='members.php'</script>";
                  }
                }else{
                  echo "<script>alert 'invalid credential, please try again'</script>";
                }
              }else{
                echo "<script>'confirm password is not matched with new password'</script>";
              }
              
            }
            ?> 

