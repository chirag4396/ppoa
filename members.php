<?php
session_start();
include("config.php");
?>
<!doctype html>

<html lang="en">
<head>
  <title> PPOA | Welcome to PPOA</title>
  
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <!-- Page Description and Author -->
  <meta name="description" content="Pawar Travels">
  <meta name="author" content="Amol">
  
  <link rel="stylesheet" href="assets/css/bootstrap.min.css" type="text/css" media="screen">

  <link rel="stylesheet" href="assets/fonts/font-awesome.min.css" type="text/css" media="screen">   
  
  <link rel="stylesheet" type="text/css" href="assets/css/main.css" media="screen">
  
  <link rel="stylesheet" type="text/css" href="assets/css/animate.css" media="screen">
  <style>
	#portfolio-list .mix {
	  padding-left: 10px!important;
	  padding-right: 10px!important;
	}

	.contact-info ul li {
	  border-bottom: 1px solid #e8e8e9;
	  float: left;
	  font-size: 12px;
	  line-height: 30px;
	  list-style: outside none none;
	  margin: 0;
	  padding: 0;
	  width: 100%;
	}

	.contact-info h3, .contact-form h3 {
	  border-bottom: 2px solid #e8e8e9 !important;
	  font-size: 16px;
	  margin: 5px 0;
	  text-transform: uppercase;
	  padding-bottom: 6px;
	}

	.description {
	  margin-bottom: 0px;
	}

	.img-container
	{
	  padding: 0px!important;
	}

  </style>

</head>
<body>
  <div id="container">
	<!-- Start Header Section -->
	<header id="header-wrap" class="site-header clearfix">
	  <!-- Start Top Bar -->
	  <div class="top-bar hidden-xs">
		<div class="container">
		  <div class="row">
			<div class="col-md-7 col-sm-9">
			  <!-- Start Contact Info -->
			  <ul class="contact-details">
				<li>
				  <a href="#">
					<i class="icon-envelope">
					</i>
					contact@pressownerspune.org
				  </a>
				</li>
				<li>
				  <a href="#">
					<i class="icon-call-out">
					</i>
					Call Us: +91-20-24471777 / 24430460
				  </a>
				</li>  
				<li>
				  <a href="#">
					<i class="icon-clock">
					</i>
					Sun-Sat <span class="timing">(9am-5pm)</span>
				  </a>
				</li>                
			  </ul>
			  <!-- End Contact Info -->
			</div>
			<div class="col-md-5 col-sm-3">
			  <!-- Start Social Links -->
			  <ul class="social-list">
				<li>
				  <a href="#" class="social-link facebook" data-toggle="tooltip" data-placement="bottom" title="Facebook" href="#"><i class="fa fa-facebook"></i></a>
				</li>
				<li>
				  <a href="#" class="social-link twitter" data-toggle="tooltip" data-placement="bottom" title="Twitter" href="#"><i class="fa fa-twitter"></i></a>
				</li>
				<li>
				  <a href="#" class="social-link google" data-toggle="tooltip" data-placement="bottom" title="Google Plus" href="#"><i class="fa fa-google-plus"></i></a>
				</li>
				
			  </ul>
			  <!-- End Social Links -->
			</div>
		  </div>
		</div>
	  </div>
	  <!-- End Top Bar -->

	  <!-- Start  Logo & Navigation  -->
	  <div class="navbar navbar-default navbar-top" role="navigation" data-spy="affix" data-offset-top="50">
		<div class="container">
		  <!-- Brand and toggle get grouped for better mobile display -->
		  <div class="navbar-header">
			<!-- Stat Toggle Nav Link For Mobiles -->
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
			  <span class="sr-only">Toggle navigation</span>
			  <span class="icon-bar"></span>
			  <span class="icon-bar"></span>
			  <span class="icon-bar"></span>
			</button>
			<!-- End Toggle Nav Link For Mobiles -->
			<div class="logo-wrapper">
			  <a class="navbar-brand" href="index.html">
				<img src="assets/img/logo.jpg" alt="ConBiz">
			  </a>  
			</div>
		  </div>
		  <!--  Brand and toggle menu for mobile ends  -->

		  <div class="navbar-collapse collapse">
			<!-- Stat Search -->
			<div class="search-side">
			  <a class="show-search">
				<i class="icon-magnifier"></i>
			  </a>
			</div>
			<!-- Form for navbar search area -->
			<form class="full-search">
			  <div class="container">
				<div class="row">
				  <input class="form-control" type="text" placeholder="Search...">
				  <a class="close-search">
					<span class="fa fa-times fa-2x">
					</span>
				  </a>
				</div>
			  </div>
			</form>
			<!-- Search form ends -->

			<!-- Start Navigation List -->
			<ul class="nav navbar-nav navbar-right">
			  <li><a href="index.html">Home </a> </li>                
			  <li><a class="active" href="#"> Company </a>
				<ul class="dropdown">
				  <li><a href="about.html">About Us</a></li>                    
				  <li><a href="history.html">Our History</a></li>
				  <li><a href="directors.html">Our Directors</a></li>
				  <li><a href="presidents.html">List of Presidents</a></li>
				  <li><a class="active" href="members.php">List of Members</a></li>                    
				</ul>
			  </li>
			  <li><a href="products.html">Products</a></li>
			  <li><a href="gallery.html">Gallery</a> </li>
			  <li><a href="events.html">Events</a></li>                
			  <li><a href="contact.html">Contact</a></li>
<!--               <li><a href="login.php">Login</a></li>   
-->                  <?php
if (isset($_SESSION['meb_id'])) {
 
  echo '<li><a href="logout.php">Logout</a></li>';

  ?>             
</ul>
<!-- End Navigation List -->
</div>
</div>
</header>
<div class="project section">
  <div class="container">
	<div class="heading">
	  <div class="section-title">Our <span>Members</span></div>              
	</div>         
  </div>
</div>

<section id="portfolio" class="section2">
  <div class="container">        
	<?php 

	
	$res = $conn->query('select * from member_details');      
	if($res->num_rows)
	{
	   //echo "<br/><a href='logout.php'>Logout</a>";
	  ?>

	  
	  <!-- Portfolio Recent Projects -->
	  <div id="portfolio-list">

	   <?php
	   while ($row_member = $res->fetch_assoc()) 
	   {
		 ?>
	 <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12 mix buildings interior">
		  <div class="our-member">
			 
                   <!--  <img src="<?php //echo 'uploads/'. $row_member['meb_image'] ?>" alt="">
                  </div> -->
			<div class="description des-left col-md-12">  
			  <div class="contact-info">
				<h3><?php echo $row_member['meb_name']; ?></h3>
				<ul>
				 <li>
				  <i class="fa fa-pencil"></i> <strong>Reg No</strong>
				  <?php echo $row_member['meb_reg']; ?>
				</li>
				<li>
				  <i class="fa fa-briefcase"></i> <strong>Company</strong>
				  <?php echo $row_member['meb_firm']; ?>
				</li>
				<li>
				  <i class="fa fa-map-marker"></i> <strong>Address</strong>
				  <?php echo $row_member['meb_address']; ?>
				</li>
				<li>
				  <i class="fa fa-mobile"></i> <strong>Mobile</strong>
				  <?php echo $row_member['meb_mobile']; ?>
				</li>
				<li>
				  <i class="fa fa-phone"></i> <strong>Phone</strong>
				  <?php echo $row_member['meb_phone']; ?>
				</li>
				<li>
				  <i class="fa fa-envelope"></i> <strong>Email</strong>
				  <a href="#"><?php echo $row_member['meb_email']; ?></a>
				</li>
			  </ul>
			</div>
		  </div>
		</div>
		
	  </div>  <?php } } ?>
	</div>
	<!-- End Portfolio  -->
  </div>

  <?php
}
else
{
  echo  '<li><a href="login.php">Login</a></li>';
  
  
  ?>
</header>
</section>



<!-- Mobile Menu Start -->
<ul class="wpb-mobile-menu">
  <li><a href="index.html">Home </a> </li>                
  <li><a class="active" href="#"> Company </a>
	<ul class="dropdown">
	  <li><a href="about.html">About Us</a></li>                    
	  <li><a href="history.html">Our History</a></li>
	  <li><a href="directors.html">Our Directors</a></li>
	  <li><a href="presidents.html">List of Presidents</a></li>
	  <li><a class="active" href="members.php">List of Members</a></li>                    
	</ul>
  </li>
  <li><a href="products.html">Products</a></li>
  <li><a href="gallery.html">Gallery</a> </li>
  <li><a href="events.html">Events</a></li>                
  <li><a href="contact.html">Contact</a></li>
  <li><a href="login.php">Login</a></li>


  
  

  <!-- Mobile Menu End -->

</div>
<!-- End Header Logo & Navigation -->
<div class="clearfix"></div>
</header>
<!-- End Header Section -->

<!-- Start Project Section -->
<div class="project section">
  <div class="container">
	<div class="heading">
	  <div class="section-title">Our <span>Members</span></div>              
	</div>         
  </div>
</div>
<?php  echo "<center><font color='red'><b>You Are Not Logged In. Please Login to See the Members List..!!!</b></font></center><br><br>";
}?>
<!-- Start Portfolio Section -->



<!-- End Portfolio Section -->


<!-- Start Footer Section -->
<footer class="footer">
  <div class="container">
	<div class="row footer-widgets">
	  <!-- Start Contact Widget -->
	  <div class="col-md-3 col-sm-6 col-xs-12">
		<div class="footer-widget contact-widget">
		  <h4>
			<img src="assets/img/logo.jpg" class="img-responsive" alt="Footer Logo" width="100%" />
		  </h4>
		  <p>PPOA Limited's Corporate Identification Number is (CIN) U22219PN1952PLC008970 and its registration number is 8970.  </p>
		</div>
	  </div>
	  <!-- End Contact Widget -->

	  <div class="col-lg-3 col-sm-4">
		<div class="footer-widget" data-wow-duration="2s" data-wow-delay=".5s">
		  <h4>
			Our Company
		  </h4>
		  <ul class="page-footer-list">
			<li>
			  <i class="fa fa-angle-right"></i>
			  <a href="history.html">Our History</a>
			</li>
			<li>
			  <i class="fa fa-angle-right"></i>
			  <a href="directors.html">Our Directors</a>
			</li>
			<li>
			  <i class="fa fa-angle-right"></i>
			  <a href="presidents.html">List of Presidents</a>
			</li>
			<li class="last-itm">
			  <i class="fa fa-angle-right"></i>
			  <a href="members.php">List of Members</a>
			</li>                
		  </ul>
		</div>
	  </div>

	  <div class="col-lg-3 col-sm-4">
		<div class="footer-widget" data-wow-duration="2s" data-wow-delay=".5s">
		  <h4>
			Our Company
		  </h4>
		  <ul class="page-footer-list">
			<li>
			  <i class="fa fa-angle-right"></i>
			  <a href="about.html">About PPOA</a>
			</li>
			<li>
			  <i class="fa fa-angle-right"></i>
			  <a href="products.html">Our Products</a>
			</li>
			<li>
			  <i class="fa fa-angle-right"></i>
			  <a href="events.html">Our Events</a>
			</li>
			<li class="last-itm">
			  <i class="fa fa-angle-right"></i>
			  <a href="gallery.html">Gallery</a>
			</li>                
		  </ul>
		</div>
	  </div>

	  <!-- Start Address Widget -->
	  <div class="col-md-3 col-sm-6 col-xs-12">
		<div class="footer-widget">
		  <h4>
			PPOA Office Address
		  </h4>
		  <ul class="address">
			<li>
			  <a href="#"><i class="fa fa-map-marker"></i><strong>Address (Regd. Office) : </strong>
				'Pune Mudran Vidyalaya Bhavan', 209/B-4, Navi Peth, Pune-411030.</a>
			  </li>
			  <li>
				<a href="#"><i class="fa fa-phone"></i> +91-20-24471777 / 24430460</a>
			  </li>
			  <li>
				<a href="#"><i class="fa fa-envelope"></i> contact@pressownerspune.org</a>
			  </li>
			</ul>
		  </div>
		</div>
		<!-- End Address Widget -->
	  </div>
	  <!-- .row -->        
	</div>
  </footer>
  <!-- End Footer Section -->

  <!-- Start Copyright -->
  <div class="copyright-section">
	<div class="container">
	  <div class="row">
		<div class="col-sm-12">
		  <p>
			Copyright &copy; 2018 PPOA - Designed & Developed by 
			<a href="http://sungare.com">
			  Sungare Technologies Pvt. Ltd.
			</a>
		  </p>
		</div>
	  </div>
	  <!-- .row -->
	</div>
  </div>
  <!-- End Copyright -->
</div>
<!-- End Full Body Container -->

<!-- Go To Top Link -->
<a href="#" class="back-to-top">
  <i class="fa fa-angle-up"></i>
</a>

<!-- Start Loader -->
<div id="loader">
  <div class="square-spin">
	<div></div>
  </div>
</div>



<!-- Main JS  -->
<script type="text/javascript" src="assets/js/jquery-2.1.4.min.js"></script>    
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="assets/js/owl.carousel.min.js"></script>  
<script type="text/javascript" src="assets/js/jquery.mixitup.min.js"></script>
<script type="text/javascript" src="assets/js/main.js"></script>

</body>
</html>